import {Service} from "typedi";
import {Request, Response} from "express";
import axios, {AxiosRequestConfig, AxiosResponse} from "axios";

@Service()
export default class BitmexService {

  public async getAllSymbols(req: Request, res: Response) {
    try {
      let query_params = {};
      let requestConfig: AxiosRequestConfig = {
        baseURL: process.env.BASE_URL,
        url: `/instrument/active`,
        method: "get",
        params: query_params
      };
      let dataCall: AxiosResponse = await axios(requestConfig);
      if (!dataCall.data || dataCall.data.length === 0) {
        return {message: [], flag: false};
      }
      return {message: dataCall.data, flag: true};
    } catch (e) {
      return {message: [], flag: false};
    }
  }

  public async getKlineData(req: Request, res: Response) {
    try {
      const {symbol, binSize} = req.query;
      let query_params = {
        "symbol": symbol,
        "binSize": binSize
      };
      let requestConfig: AxiosRequestConfig = {
        baseURL: process.env.BASE_URL,
        url: `/trade/bucketed`,
        method: "get",
        params: query_params
      };
      let dataCall: AxiosResponse = await axios(requestConfig);
      if (!dataCall.data || dataCall.data.length === 0) {
        return {message: [], flag: false};
      }
      return {message: dataCall.data, flag: true};
    } catch (e) {
      return {message: [], flag: false};
    }
  }


}
