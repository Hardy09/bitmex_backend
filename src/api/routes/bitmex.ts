import {NextFunction, Request, Response, Router} from "express";
import {Container} from "typedi";
import BitmexService from "@/services/bitmexService";
import {celebrate, Joi} from "celebrate";

const route = Router();

export default (app: Router) => {

  app.use('/bitmex', route);

  route.get('/symbols', async (req: Request, res: Response) => {
      const result: { message: string | object; flag: boolean } = await Container.get(BitmexService)
        .getAllSymbols(req, res);
      if (result.flag) {
        return res.status(200).json({success: true, result: {message: result.message}});
      } else {
        return res.status(200).json({success: false, result: {error: result.message}});
      }
    },
  );

  route.get('/symbol/kline', celebrate({
      query: Joi.object().keys({
        symbol: Joi.string().required(),
        binSize: Joi.string().required().valid('1m').valid('5m').valid('1h').valid('1d')
      }),
    }), async (req: Request, res: Response) => {
      const result: { message: string | object; flag: boolean } = await Container.get(BitmexService)
        .getKlineData(req, res);
      if (result.flag) {
        return res.status(200).json({success: true, result: {message: result.message}});
      } else {
        return res.status(200).json({success: false, result: {error: result.message}});
      }
    },
  );

}
